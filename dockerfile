# Alpine Linux with OpenJDK JRE
FROM openjdk:8-jre-alpine
# copy WAR into image
COPY target/member-sync-profile.jar /member-sync-profile.jar
# run application with this command line[
CMD ["java", "-jar", "/member-sync-profile.jar"]
RUN apk add --no-cache tzdata
ENV TZ=Asia/Jakarta
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone
